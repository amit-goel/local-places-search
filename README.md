# Local Search using fourSquare API

Sample Application which performs searches against FourSquare API

![Scheme](images/search.png) ----- ![Scheme](images/detail.png) ----- ![Scheme](images/map.png)

## Getting Started

Clone the project and run in Android Studio.  Gradle plugin version 3.1+.  Project uses code in Kotlin.

### Technical features

* **Kotlin**

* **Architecture Components**

* **RxJava**

* **Dagger DI framework**

* **MVVM Architecture with viewModel**

* **Retrofit, Picasso, and Timber**

* **Google Maps Static API**

## Notes

Details page relies heavily on hitting the venues/id FourSquare Endpoint, which is a premium service.

Detail page request will have limited information and styling if 50 max hits are hit per day.


## Authors

* **Amit Goel** - *Initial work*

## License

This project is licensed under the MIT License