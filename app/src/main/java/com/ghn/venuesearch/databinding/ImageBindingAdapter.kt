package com.ghn.venuesearch.databinding

import android.databinding.BindingAdapter
import android.graphics.Color
import android.graphics.Typeface
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun ImageView.imageUrl(url: String?) = url?.isNotBlank().let{Picasso.get().load(url).into(this)}

@BindingAdapter("imageUrlFit")
fun ImageView.imageUrlFit(url: String?) = url?.let{
    if(it.isNotEmpty()) Picasso.get().load(url).fit().into(this)
}

@BindingAdapter("visibleOrGone")
fun View.visibleOrGone(visible: String?) {
    visibility = if(!TextUtils.isEmpty(visible)) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleOrInvisible")
fun View.visibleOrInvisible(visible: String?) {
    visibility = if(!TextUtils.isEmpty(visible)) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("colorText")
fun TextView.colorText(value: String?) {
    if(value!= null && value != "N/A")
        value.apply {
            setTextColor(Color.parseColor("#$value"))
            setTypeface(null, Typeface.BOLD)
        }

}
