package com.ghn.venuesearch.di

import com.ghn.venuesearch.viewModel.DetailActivityViewModel
import com.ghn.venuesearch.viewModel.MainActivityViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class), (PrefsModule::class)])
interface AppComponent {

    fun inject(viewModel: MainActivityViewModel)
    fun inject(viewModel: DetailActivityViewModel)

}