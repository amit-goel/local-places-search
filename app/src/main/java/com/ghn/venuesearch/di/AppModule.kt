package com.ghn.venuesearch.di

import android.content.Context
import com.ghn.venuesearch.HomeAwayApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module which provides all required dependencies about Context
 */
@Module
class AppModule (private val application: HomeAwayApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = application
}