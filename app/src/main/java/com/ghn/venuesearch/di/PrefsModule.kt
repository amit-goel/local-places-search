package com.ghn.venuesearch.di

import android.content.Context
import com.ghn.venuesearch.repository.SharedPrefsDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PrefsModule{
    @Provides
    @Singleton
    fun provideSharedPrefsDataSource(context: Context) =
            SharedPrefsDataSource.getPrefs(context)
}