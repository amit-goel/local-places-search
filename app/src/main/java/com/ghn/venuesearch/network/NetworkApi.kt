package com.ghn.venuesearch.network

import com.ghn.venuesearch.models.FSResponse
import com.ghn.venuesearch.models.VenueResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkApi {

    @GET("venues/search")
    fun places(@Query("query") query: String,
               @Query("near") near: String,
               @Query("limit") limit: Int = 20,
               @Query("v") v: String = "20180401"
    ): Observable<Response<FSResponse>>

    @GET("venues/{id}")
    fun place(@Path("id") id: String,
              @Query("v") v: String = "20180401"): Observable<Response<VenueResponse>>
}