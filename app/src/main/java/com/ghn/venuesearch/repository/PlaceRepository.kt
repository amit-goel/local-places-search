package com.ghn.venuesearch.repository

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.SharedPreferences
import com.ghn.venuesearch.models.FSResponse
import com.ghn.venuesearch.network.NetworkApi
import com.ghn.venuesearch.models.VenueResponse
import com.ghn.venuesearch.util.LOCATION_NEAR
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.Response
import javax.inject.Inject

class PlaceRepository @Inject constructor(private val api: NetworkApi,
                                          private val prefs: SharedPreferences,
                                          private val context: Context) : Repository {

    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()
    private val compositeDisposable = CompositeDisposable()

    override fun getPlaces(): MutableLiveData<List<FSResponse>> {
        val mutableLiveData = MutableLiveData<List<FSResponse>>()
        //room structure to return live data if using Room SQL
        /*val disposable = room.storeDao().getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ mutableLiveData.value = it },
                        { t: Throwable? -> t?.printStackTrace() })
        allCompositeDisposable.add(disposable)*/
        return mutableLiveData
    }

    fun getPlacesApi(query: String): Observable<Response<FSResponse>> =
            api.places(near = LOCATION_NEAR, query = query)

    fun getPlaceApi(id: String): Observable<Response<VenueResponse>> = api.place(id = id)

    fun getPrefsFavorite(id: String) = prefs.getBoolean(id, false)

    fun writePrefs(id: String?, checked: Boolean = false) {
        if (getPrefsFavorite(id ?: "") && !checked) prefs.edit().remove(id).apply()
        else prefs.edit().putBoolean(id, checked).apply()
    }

}