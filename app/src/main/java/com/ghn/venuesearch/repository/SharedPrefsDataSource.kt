package com.ghn.venuesearch.repository

import android.content.Context
import android.content.SharedPreferences

class SharedPrefsDataSource {

    companion object {

        fun getPrefs(context: Context): SharedPreferences{
           return context.getSharedPreferences("PrefName",Context.MODE_PRIVATE)
        }
    }
}