package com.ghn.venuesearch.state

import android.os.Parcelable
import com.ghn.venuesearch.models.Venue
import kotlinx.android.parcel.Parcelize

data class ItemListViewState(
        val toolbarTitle: String,
        val items: List<ItemRow>)

@Parcelize
data class ItemRow(val venue: Venue) : Parcelable