package com.ghn.venuesearch.util

import android.content.res.Resources

fun formatPhoneNumber(phone: String?): String {
    val format = phone?.let { "(${it.substring(0, 3)}) ${it.substring(3, 6)} - ${it.substring(6, 10)}" }
    return format ?: "Unavailable"
}

fun convertDpToPixel(dp: Float): Float {
    val px = dp * (Resources.getSystem().displayMetrics.densityDpi / 160f)
    return Math.round(px).toFloat()
}