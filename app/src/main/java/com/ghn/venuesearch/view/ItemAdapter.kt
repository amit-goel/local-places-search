package com.ghn.venuesearch.view

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ghn.venuesearch.BR
import com.ghn.venuesearch.R
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.state.ItemRow
import kotlinx.android.synthetic.main.item_row.view.*
import java.util.*

class ItemAdapter(private val listener: ItemAdapterListener) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private val items = ArrayList<ItemRow>()

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(row: ItemRow) {
            binding.setVariable(BR.item, row)
            binding.executePendingBindings()
        }
    }

    interface ItemAdapterListener {
        fun onItemClicked(venue: Venue)
        fun onFavoriteClicked(venue: Venue, checked: Boolean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.item_row, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { listener.onItemClicked(items[position].venue) }
        holder.itemView.favoriteToggle.setOnCheckedChangeListener { cb, checked ->
            if(cb.isPressed)
                listener.onFavoriteClicked(items[position].venue, checked)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(newItems: List<ItemRow>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<ItemRow> {
        return items
    }
}
